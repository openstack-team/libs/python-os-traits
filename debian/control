Source: python-os-traits
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-coverage,
 python3-hacking,
 python3-openstackdocstheme,
 python3-oslotest,
 python3-stestr,
 python3-testscenarios,
 python3-testtools,
 subunit,
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/openstack-team/libs/python-os-traits
Vcs-Git: https://salsa.debian.org/openstack-team/libs/python-os-traits.git
Homepage: http://www.openstack.org/

Package: python-os-traits-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: standardized trait strings - doc
 Os-traits is a library containing standardized trait strings.
 .
 Traits are strings that represent a feature of some resource provider.  This
 library contains the catalog of constants that have been standardized in the
 OpenStack community to refer to a particular hardware, virtualization,
 storage, network, or device trait.
 .
 This package contains the documentation.

Package: python3-os-traits
Architecture: all
Depends:
 python3-pbr,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-os-traits-doc,
Description: standardized trait strings - Python 3.x
 Os-traits is a library containing standardized trait strings.
 .
 Traits are strings that represent a feature of some resource provider.  This
 library contains the catalog of constants that have been standardized in the
 OpenStack community to refer to a particular hardware, virtualization,
 storage, network, or device trait.
 .
 This package contains the Python 3.x module.
